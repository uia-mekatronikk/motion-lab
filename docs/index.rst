.. motion-lab documentation master file, created by
   sphinx-quickstart on Thu Jun 01 12:56:33 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for the Norwegian Motion laboratory
=================================================

.. image:: ./img/theNorwegianMotionLab.jpg
    :alt: The Norwegian Motion Laboratory at a glance.

.. toctree::
    :maxdepth: 2
    :caption: Contents:
   
    remote-iface
    comm
    comau
    stewart-platforms
    publications

Indices and tables
==================


* :ref:`modindex`
* :ref:`search`
