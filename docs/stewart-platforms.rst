.. _stewart-platforms:

Stewart Platforms
#################

Kinematics
==========


The orientation and position of the Stewart platforms are given as:

.. math::
    {p}^n_{b/n} = 
    \begin{bmatrix}
    x \\ 
    y \\
    z
    \end{bmatrix} \in \mathbb{R}^3
    \hspace{10mm}
    {\Theta}_{nb} =
    \begin{bmatrix}
    \phi \\
    \theta \\
    \psi
    \end{bmatrix} \in SO(3)
    
where the notation used :math:`{p}^n_{b/n}` is the position of the Stewart platform's body frame :math:`\{b\}` relative to the inertal frame :math:`\{n\}`, given in the inertial frame :math:`\{n\}`. The velocites and accelerations share the same notation and are defined as:

.. math::
    {v}^b_{b/n} =
    \begin{bmatrix}
    u \\
    v \\
    w
    \end{bmatrix} \in \mathbb{R}^3
    \hspace{10mm}
    {\omega}^b_{b/n} =
    \begin{bmatrix}
    p \\
    q \\
    r
    \end{bmatrix} \in \mathbb{R}^3

The Euler angle sequence is defined by conventional rotation matrices. The explicit rotation sequence is given as:

.. math::
    {R}^n_b({\Theta}_{nb}) = {R}_x(\phi){R}_y(\theta){R}_z(\psi)
    \hspace{5mm} \text{or} \hspace{5mm}
    {R}_z(\psi){R}_y(\theta){R}_x(\phi)

The accompanying transformation between time derivatives of the Euler angles :math:`\dot{{\Theta}}_{nb}` and the local body-fixed rotational velocities :math:`{\omega}^b_{b/n}` is given by the following two equations:

.. math::
    \dot{{\Theta}}_{nb} = {T}_\Theta({\Theta}_{nb}){\omega}^b_{b/n}

.. math::
    {\omega}^b_{b/n} =
    ({R}_x(\phi){R}_y(\theta){R}_z(\psi))^T
    \begin{bmatrix}
    \dot{\phi} \\ 0 \\ 0
    \end{bmatrix}
    + ({R}_y(\theta){R}_z(\psi))^T
    \begin{bmatrix}
    0 \\ \dot{\theta} \\ 0
    \end{bmatrix}
    + 
    \begin{bmatrix}
    0 \\ 0 \\ \dot{\psi}
    \end{bmatrix} \\


